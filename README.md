# C64 SID sound Chip Features Demo
Some features od the SIS chip are shown on the video.   
I am not very good at making chip tunes so I just played with each feature independently. Not all features are covered line filters and multiple channels.
# Program Description AKA TODO 
- [x] Set to ok settings
- [x] Frequency sweep up
   - [x] Diplay Values
- [x] Set to ok frequency
   - [x] Diplay Values
- [x] Volume sweep UP
   - [x] Diplay Values
   - [x] up
   - [x] down
   - [x] set to ok volume after it
- [x] Triangular wave
- [x] Sawtooth wave
- [x] Triangular+Sawtooth  wave
- [x] noise
- [x] Square (pwm) wave
    - [x] Width sweep


# Project Dependencies
- Vice Emulator  
- TOK64 

# Text to Basic
Use custom made script to add line number automatically.
```bash
./numbercode <code file> <output file w numbs>
```


# Install Vice Emulator
- download source code from vice website  
- dependencies(debian):
```bash
sudo apt-get install build-essential autoconf
sudo apt-get install bison flex libreadline-dev libxaw7-dev libpng-dev xa65 texinfo libpulse-dev texi2html libpcap-dev dos2unix libgtk2.0-cil-dev libgtkglext1-dev libvte-dev libvte-dev libavcodec-dev libavformat-dev libswscale-dev libmp3lame-dev libmpg123-dev yasm ffmpeg libx264-dev
```
- compile:
if `./configure` not in folder then generate by running `./autogen.sh`   
```bash
./configure && make && sudo make install
```
# Basic to PRG
To convert basix language to a prg file that can be passed to the emulator then we have to use TOK64 to convert to .PRG format.  

If in Linux then we need to either compile it or run a windows binary in `DoSBox`.  

## TOK64 in DoSBox
- Install `dosbox` from the repositories
- Run `dosbox` in the terminal
- in the `dosbox` terminal execute `mount c /path/to/mount/as/C/drive`
- then execute `C:` to change to the c drive
- then run `TOK64.exe mainBasicCode.txt`
- it will convert the code to .prg
- to run in emulator exit `dosbox` and then execute `x64sc file.prg`

# Use PRG Files in Vice

## Linux
```bash
./viceCommand file.prg
```

## Windows
follow this [tutotial](https://wpguru.co.uk/2014/06/how-to-load-a-prg-file-in-vice/).

# Basi Clear Screen
```basic
PRINT CHR$(147)
```

# Good Sources of Information
[C64 Wiki](https://www.c64-wiki.com/wiki/Main_Page)   
[BASIC - C64 Wiki](https://www.c64-wiki.com/wiki/BASIC)    
[SID - C64 Wiki](https://www.c64-wiki.com/wiki/SID)   
